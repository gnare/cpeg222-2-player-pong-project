/* ************************************************************************** */
/** common.h

  @Author
    Galen Nare

  @File Name
    common.h

  @Summary
     Common header for project 2

     You may not distribute or modify this code without explicit, written permission from its author.
 */
/* ************************************************************************** */

#ifndef _COMMON_HEADER    /* Guard against multiple inclusion */
#define _COMMON_HEADER

    /*------------------ Board system settings. PLEASE DO NOT MODIFY THIS PART ----------*/
    #pragma config FPLLIDIV = DIV_2         // PLL Input Divider (2x Divider)
    #pragma config FPLLMUL = MUL_20         // PLL Multiplier (20x Multiplier)
    #pragma config FPLLODIV = DIV_1         // System PLL Output Clock Divider (PLL Divide by 1)
    #pragma config FNOSC = PRIPLL           // Oscillator Selection Bits (Primary Osc w/PLL (XT+,HS+,EC+PLL))
    #pragma config FSOSCEN = OFF            // Secondary Oscillator Enable (Disabled)
    #pragma config POSCMOD = XT             // Primary Oscillator Configuration (XT osc mode)
    #pragma config FPBDIV = DIV_8           // Peripheral Clock Divisor (Pb_Clk is Sys_Clk/8)
    /*----------------------------------------------------------------------------*/

    #include <stdlib.h>
    #include <stdio.h>
    #include <xc.h>     // Microchip XC processor header which links to the PIC32MX370512L header
    #include "config.h" // Basys MX3 configuration header
    #include "lcd.h"
    #include "ssd.h"

    #define SYS_FREQ (80000000L) // 80MHz system clock
    #define MS_CONSTANT 1426

    void delay_ms(int);        // Function prototypes
    void lcd_display_start();
    void lcd_display_update();
    void lcd_print(char*);
    void lcd_score(int);
    
    int checkWhoServe(unsigned int, unsigned int);
    int gameOverCheck(int, int);

#endif /* _COMMON_HEADER */

#ifndef _SUPPRESS_PLIB_WARNING          // suppress the plib warning during compiling
        #define _SUPPRESS_PLIB_WARNING      
#endif

/* *****************************************************************************
 End of File
 */
