/*===================================CPEG222====================================
 * Program:		project2main.c
 * Authors: 	Galen Nare
 * Date: 		09/30/2020
 * Description: A 2 player pong game using the buttons and LEDs
 * Input: Button press (multi)
 * Output: LED array - field/ ball position
 *         SSD       - scoreboard
 *         LCD       - game status
 * 
 * You may not distribute or modify this code without explicit, written permission from its author.
==============================================================================*/

#include "common.h"

const int delay = 50;

unsigned int score_p1 = 0, score_p2 = 0;

int main(void) {
    int LD_position = 0;
    DDPCONbits.JTAGEN = 0; // Statement is required to use Pin RA0 as IO
    TRISA &= 0xFF00; // Set Port A bits 0~7 to 0, i.e., LD 0~7 are configured as digital output pins
    LATA = LD_position; // write the hex value of LD_position to the 8 LEDs
    
    TRISBbits.TRISB0 = 1; // RB1 (BTNL) configured as input
    ANSELBbits.ANSB0 = 0; // RB1 (BTNL) disabled analog
    
    TRISBbits.TRISB8 = 1; // RB8 (BTNR) configured as input
    ANSELBbits.ANSB8 = 0;	// RB8 (BTNR) disabled analog
    
    SSD_Init();
    LCD_Init(); // Init and clear the drivers for the LCD and SSD.
    
    int ssd1_tens = score_p1 / 10, ssd1_ones = score_p1 % 10; // Split the numbers for the SSD.
    int ssd2_tens = score_p2 / 10, ssd2_ones = score_p2 % 10;

    SSD_WriteDigits(ssd2_ones, ssd2_tens, ssd1_ones, ssd1_tens,
        0, 0, 0, 0); // Write the score to the SSD
    
    lcd_display_start(); // Show the mode 1 message on the LCD
    
    int p_scored = -1; // Track the scoring player
    int mode = 1; // 1 = Initial/Game over, 2 = Ready to Serve, 3 = Right Shift, 4 = Left Shift, 5 = Score
    int cycle_counter = 0; // Timer variable
    
    int buttonLock_L = 0, buttonLock_R = 0; // Button state variables 
    int btnL = 0, btnR = 0;
    
    while (1) {
        btnL = PORTBbits.RB0;		// read BTNL
        btnR = PORTBbits.RB8;		// read BTNR
        
        cycle_counter++; // Increment the timing variable
        
        if (score_p1 > 99) { // Don't overflow the SSD.
            score_p1 = 0;
        }
        
        if (score_p2 > 99) { // Don't overflow the SSD.
            score_p2 = 0;
        }
        
        if (btnL && !buttonLock_L && mode != 3) // Actions when left button is pressed
        {
            delay_ms(delay); // wait 20ms, or you can change the value of delay
            buttonLock_L = 1; // Lock button
            
            switch (mode) {
                case 1: // Initial/game over
                    score_p1 = 0, score_p2 = 0;
                    cycle_counter = 0;
                    mode = 2;
                    lcd_display_update();
                    
                    int ssd1_tens = score_p1 / 10, ssd1_ones = score_p1 % 10;
                    int ssd2_tens = score_p2 / 10, ssd2_ones = score_p2 % 10;

                    SSD_WriteDigits(ssd2_ones, ssd2_tens, ssd1_ones, ssd1_tens,
                    0, 0, 0, 0);
                    break;

                    LATA = LD_position; // write the hex value of LD_position to the 8 LEDs
                case 2: // Ready to serve
                    if (!checkWhoServe(score_p1, score_p2)) {
                        mode = 3; // Left serves
                        cycle_counter = 0;
                    }
                    
                    ssd1_tens = score_p1 / 10, ssd1_ones = score_p1 % 10;
                    ssd2_tens = score_p2 / 10, ssd2_ones = score_p2 % 10;

                    SSD_WriteDigits(ssd2_ones, ssd2_tens, ssd1_ones, ssd1_tens,
                    0, 0, 0, 0);
                    break;
                case 3: // Right shift
                    break;
                case 4: // Left shift
                    if (LD_position == 1 << 7) { // Bounce, swap to mode 3
                        mode = 3;
                    } else { // Too early, right score
                        score_p2++;
                        cycle_counter = 0;
                        mode = 5;
                        p_scored = 1;
                    }
                    break;
                case 5: // Score (no button response here)
                    break;
            }
            
        } else if (buttonLock_L && !btnL) { // Actions when the left button is released
            delay_ms(delay); // wait 20ms, or you can change the value of delay
            // to make the button more/less sensitive
            buttonLock_L = 0; // unlock buttons to allow further press being recognized
        }
        
        if (btnR && !buttonLock_R && mode != 4) // Actions when right button is pressed
        {
            delay_ms(delay); // wait 20ms, or you can change the value of delay
            buttonLock_R = 1; // Lock button
            
            switch (mode) {
                case 1: // Initial/game over
                    score_p1 = 0, score_p2 = 0;
                    cycle_counter = 0;
                    mode = 2;
                    lcd_display_update();
                    
                    int ssd1_tens = score_p1 / 10, ssd1_ones = score_p1 % 10;
                    int ssd2_tens = score_p2 / 10, ssd2_ones = score_p2 % 10;

                    SSD_WriteDigits(ssd2_ones, ssd2_tens, ssd1_ones, ssd1_tens,
                    0, 0, 0, 0);
                    break;

                    LATA = LD_position; // write the hex value of LD_position to the 8 LEDs
                case 2: // Ready to serve
                    if (checkWhoServe(score_p1, score_p2)) {
                        mode = 4; // Right serves
                        cycle_counter = 0;
                    }
                    
                    ssd1_tens = score_p1 / 10, ssd1_ones = score_p1 % 10;
                    ssd2_tens = score_p2 / 10, ssd2_ones = score_p2 % 10;

                    SSD_WriteDigits(ssd2_ones, ssd2_tens, ssd1_ones, ssd1_tens,
                    0, 0, 0, 0);
                    break;
                case 3: // Right shift
                    if (LD_position == 1) { // bounce, swap to mode 4
                        mode = 4;
                    } else { // To early, left scores
                        score_p1++;
                        cycle_counter = 0;
                        mode = 5;
                        p_scored = 0;
                    }
                    break;
                case 4: // Left shift
                    break;
                case 5: // Score (no button response here)
                    break;
            }
            
        } else if (buttonLock_R && !btnR) { // Actions when the right button is released
            delay_ms(delay); // wait 20ms, or you can change the value of delay
            // to make the button more/less sensitive
            buttonLock_R = 0; // unlock buttons to allow further press being recognized
        }
        
        switch(mode) {
            case 1:
                lcd_display_start();
                break;
            case 2:
                if (checkWhoServe(score_p1, score_p2)) {
                    LD_position = 1;
                } else {
                    LD_position = 1 << 7;
                }
                LATA = LD_position; // write the hex value of LD_position to the 8 LEDs
                break;
            case 3:
                if (cycle_counter >= MS_CONSTANT * 30) {
                    if (LD_position == 1) {
                        score_p1++;
                        cycle_counter = 0;
                        mode = 5;
                        p_scored = 0;
                    } else {
                        LD_position >>= 1;
                        LATA = LD_position; // write the hex value of LD_position to the 8 LEDs
                    }
                    cycle_counter = 0;
                }
                break;
            case 4:
                if (cycle_counter >= MS_CONSTANT * 30) {
                    if (LD_position == 1 << 7) {
                        score_p2++;
                        cycle_counter = 0;
                        mode = 5;
                        p_scored = 1;
                    } else {
                        LD_position <<= 1;
                        LATA = LD_position; // write the hex value of LD_position to the 8 LEDs
                    }
                    cycle_counter = 0;
                }
                break;
            case 5:
                LD_position = 0;
                LATA = LD_position; // write the hex value of LD_position to the 8 LEDs
                lcd_score(p_scored);
                p_scored = -1;
                
                int ssd1_tens = score_p1 / 10, ssd1_ones = score_p1 % 10;
                int ssd2_tens = score_p2 / 10, ssd2_ones = score_p2 % 10;
                
                SSD_WriteDigits(ssd2_ones, ssd2_tens, ssd1_ones, ssd1_tens,
                    0, 0, 0, 0);
                
                delay_ms(1500);
                lcd_display_update();
                if (gameOverCheck(score_p1, score_p2) > 0) {
                    mode = 1;
                } else {
                    mode = 2;
                }
                break;
        }
        
    }
}

/* ----------------------------------------------------------------------------- 
 **	delay_ms
 **	Parameters:
 **		ms - amount of milliseconds to delay (based on 80 MHz SSCLK)
 **	Return Value:
 **		none
 **	Description:
 **		Create a delay by counting up to counter variable
 ** -------------------------------------------------------------------------- */
void delay_ms(int ms) {
    int i, counter;
    for (counter = 0; counter < ms; counter++) {
        for (i = 0; i < MS_CONSTANT; i++) {
        } //software delay 1 milliseconds
    }
}


/* ----------------------------------------------------------------------------- 
 **	checkWhoServe
 **	Parameters:
 **		score_left - left player score
 **     score_right - right player score
 **	Return Value:
 **		0 - Left player serves
 **     1 - Right player serves
 **	Description:
 **		Check which player is serving the ball
 ** -------------------------------------------------------------------------- */
int checkWhoServe(unsigned int score_left, unsigned int score_right) {
    int total = score_left + score_right;
    if (total > 0) {
        return total / 2 % 2 == 1;
    } else {
        return 0;
    }
}


/* ----------------------------------------------------------------------------- 
 **	gameOverCheck
 **	Parameters:
 **		score_left - left player score
 **     score_right - right player score
 **	Return Value:
 **		0 - No winner yet
 **     1 - Left player wins
 **     2 - Right player wins
 **	Description:
 **		Check if the game is over and if so, who wins
 ** -------------------------------------------------------------------------- */
int gameOverCheck(int score_left, int score_right) {
    if (score_left >= 11 && score_right >= 11) {
        if (score_left - score_right >= 2) {
            return 1;
        } else if (score_right - score_left >= 2) {
            return 2;
        } else {
            return 0;
        }
    } else if (score_left >= 11 && score_left - score_right >= 2) {
        return 1;
    } else if (score_right >= 11 && score_right - score_left >= 2) {
        return 2;
    } else {
        return 0;
    }
}


/* ----------------------------------------------------------------------------- 
 **	lcd_print
 **	Parameters:
 **		str - the string to print
 **	Return Value:
 **		nothing
 **	Description:
 **		Print a string to the LCD at pos 0
 ** -------------------------------------------------------------------------- */
void lcd_print(char* str) {
    LCD_DisplayClear();
    delay_ms(5);
    LCD_WriteStringAtPos(str, 0, 0);
}


/* ----------------------------------------------------------------------------- 
 **	lcd_display_start
 **	Parameters:
 **		none
 **	Return Value:
 **		nothing
 **	Description:
 **		Print the mode 1 display to the LCD
 ** -------------------------------------------------------------------------- */
void lcd_display_start() {
    int winner = gameOverCheck(score_p1, score_p2);
    if (winner == 0) {
        LCD_WriteStringAtPos("Welcome to  ", 0, 0);
        LCD_WriteStringAtPos("a new game     ", 1, 0);
    } else if (winner == 1) {
        LCD_WriteStringAtPos("Game Over!  ", 0, 0);
        LCD_WriteStringAtPos("Congrats Left! ", 1, 0);
    } else if (winner == 2) {
        LCD_WriteStringAtPos("Game Over!  ", 0, 0);
        LCD_WriteStringAtPos("Congrats Right!", 1, 0);
    }
}


/* ----------------------------------------------------------------------------- 
 **	lcd_score
 **	Parameters:
 **		player - the number of the player that scored (0 - left, 1 - right)
 **	Return Value:
 **		nothing
 **	Description:
 **		Print the mode 5 display to the LCD
 ** -------------------------------------------------------------------------- */
void lcd_score(int player) {
    if (!player) {
        LCD_WriteStringAtPos("Left Scored!  ", 0, 0);
    } else {
        LCD_WriteStringAtPos("Right Scored! ", 0, 0);
    }
    
    if (checkWhoServe(score_p1, score_p2)) {
        LCD_WriteStringAtPos("Serve = Right ", 1, 0);
    } else {
        LCD_WriteStringAtPos("Serve = Left  ", 1, 0);
    }
}


/* ----------------------------------------------------------------------------- 
 **	lcd_display_update
 **	Parameters:
 **		none
 **	Return Value:
 **		nothing
 **	Description:
 **		Print the mode 2, 3, or 4 display to the LCD
 ** -------------------------------------------------------------------------- */
void lcd_display_update() {
    if (checkWhoServe(score_p1, score_p2)) {
        LCD_WriteStringAtPos("Serve = Right ", 1, 0);
    } else {
        LCD_WriteStringAtPos("Serve = Left  ", 1, 0);
    }
    LCD_WriteStringAtPos("Game in play", 0, 0);
}
